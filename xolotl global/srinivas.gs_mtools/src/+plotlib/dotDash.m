% makes a dot-dash plot
% where means are indicated by dots and standard deviations
% by dashes

function dotDash(data, X)


M =  mean(data,2, 'omitnan' );
S = std(data,[],2, 'omitnan' );



plot(1:length(M),M,'.','MarkerSize',10)
